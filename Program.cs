﻿using RFID_Reader_Cmds;
using RFID_Reader_Com;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace ConsoleReader
{

	class Program
	{
		public static ReceiveParser rp;
		static int FailEPCNum = 0;
		static int SucessEPCNum = 0;
		static double errnum = 0;
		static double db_errEPCNum = 0;
		static double db_LoopNum_cnt = 0;
		static string per = "0.000";
		static string AntennaGain = "";
		static string Coupling = "";
		static string pc = string.Empty;
		static string epc = string.Empty;
		static string crc = string.Empty;
		static string rssi = string.Empty;
		private static string hardwareVersion;
		static void Main(string[] args)
		{

			rp = new ReceiveParser();
			Sp.GetInstance().ComDevice.DataReceived += new SerialDataReceivedEventHandler(rp.DataReceived);

			rp.PacketReceived += new EventHandler<StrArrEventArgs>(rp_PaketReceived);

			Sp.GetInstance().Config("COM6", 115200, Parity.None, 8, StopBits.One);
			if (!Sp.GetInstance().Open())
			{
				Console.WriteLine("Port Not Opened!");
			}

			checkReaderAvailable();

			Thread.Sleep(500);
			string MultiSendSend = Commands.BuildReadMultiFrame(65535);
			Sp.GetInstance().Send(MultiSendSend);
			Console.ReadKey();
		}

		private static void rp_PaketReceived(object sender, StrArrEventArgs e)
		{

			string[] packetRx = e.Data;
			string strPacket = string.Empty;

			for (int i = 0; i < packetRx.Length; i++)
			{
				strPacket += packetRx[i] + " ";
			}

			if (packetRx[1] == ConstCode.FRAME_TYPE_INFO && packetRx[2] == ConstCode.CMD_INVENTORY)         //Succeed to Read EPC
			{

				SucessEPCNum = SucessEPCNum + 1;
				db_errEPCNum = FailEPCNum;
				db_LoopNum_cnt = db_LoopNum_cnt + 1;
				errnum = (db_errEPCNum / db_LoopNum_cnt) * 100;
				per = string.Format("{0:0.000}", errnum);

				int rssidBm = Convert.ToInt16(packetRx[5], 16); // rssidBm is negative && in bytes
				if (rssidBm > 127)
				{
					rssidBm = -((-rssidBm) & 0xFF);
				}

				AntennaGain = "3";
				Coupling = "-20";
				rssidBm -= Convert.ToInt32("-20", 10);
				rssidBm -= Convert.ToInt32("3", 10);
				rssi = rssidBm.ToString();

				int PCEPCLength = ((Convert.ToInt32((packetRx[6]), 16)) / 8 + 1) * 2;
				pc = packetRx[6] + " " + packetRx[7];
				epc = string.Empty;
				for (int i = 0; i < PCEPCLength - 2; i++)
				{
					epc = epc + packetRx[8 + i];
				}

				epc = Commands.AutoAddSpace(epc);
				crc = packetRx[6 + PCEPCLength] + " " + packetRx[7 + PCEPCLength];

				Thread.Sleep(20);
				select();
				string txtSend = Commands.BuildReadDataFrame("00000000", 2, 0, 4);
				Sp.GetInstance().Send(txtSend);

			}
			else if (packetRx[2] == ConstCode.CMD_GET_MODULE_INFO)
			{
				string txtHardwareVersion = "";
				string txtFirmwareVersion = "";

				if (packetRx[5] == ConstCode.MODULE_HARDWARE_VERSION_FIELD)
				{
					hardwareVersion = String.Empty;
					try
					{
						for (int i = 0; i < Convert.ToInt32(packetRx[4], 16) - 1; i++)
						{
							hardwareVersion += (char)Convert.ToInt32(packetRx[6 + i], 16);
						}
						txtHardwareVersion = hardwareVersion;
						getFirmwareVersion();

						Thread.Sleep(100);
						getModuleRegion();
					}
					catch (System.Exception ex)
					{
						hardwareVersion = packetRx[6].Substring(1, 1) + "." + packetRx[7];
						txtHardwareVersion = hardwareVersion;
						Console.WriteLine(ex.Message);
					}
				}
				else if (packetRx[5] == ConstCode.MODULE_SOFTWARE_VERSION_FIELD)
				{
					String firmwareVersion = string.Empty;
					try
					{
						for (int i = 0; i < Convert.ToInt32(packetRx[4], 16) - 1; i++)
						{
							firmwareVersion += (char)Convert.ToInt32(packetRx[6 + i], 16);
						}
						txtFirmwareVersion = firmwareVersion;
					}
					catch (System.Exception ex)
					{
						txtFirmwareVersion = "";
						Console.WriteLine(ex.Message);
					}
				}

			}
			else if (packetRx[2] == ConstCode.CMD_READ_DATA)         //Read Tag Memory
			{
				string strInvtReadData = "";
				string txtInvtRWData = "";
				string txtOperateEpc = "";
				int dataLen = Convert.ToInt32(packetRx[3], 16) * 256 + Convert.ToInt32(packetRx[4], 16);
				int pcEpcLen = Convert.ToInt32(packetRx[5], 16);

				for (int i = 0; i < pcEpcLen; i++)
				{
					txtOperateEpc += packetRx[i + 6] + " ";
				}

				for (int i = 0; i < dataLen - pcEpcLen - 1; i++)
				{
					strInvtReadData = strInvtReadData + packetRx[i + pcEpcLen + 6];
				}
				txtInvtRWData = Commands.AutoAddSpace(strInvtReadData);
				Console.WriteLine("TID:" + txtInvtRWData);
			}



		}

		public static void checkReaderAvailable()
		{

			if (Sp.GetInstance().IsOpen())
			{
				Sp.GetInstance().Send(Commands.BuildGetModuleInfoFrame(ConstCode.MODULE_HARDWARE_VERSION_FIELD));
			}
		}

		private static void select()
		{
			if (Sp.GetInstance().IsOpen() == false)
			{
				return;
			}
			int intSelTarget = 0;
			int intAction = 0;
			int intSelMemBank = 1;

			int intSelPointer = 32;
			string txtSelLength = (epc.Replace(" ", "").Length * 4).ToString("X2");
			int intMaskLen = Convert.ToInt32(txtSelLength, 16);
			int intSelDataMSB = 1;
			int intTruncate = 0;

			Sp.GetInstance().Send(Commands.BuildSetSelectFrame(intSelTarget, intAction, intSelMemBank, intSelPointer, intMaskLen, intTruncate, epc));
			Thread.Sleep(20);

		}
		private static void getFirmwareVersion()
		{
			Sp.GetInstance().Send(Commands.BuildGetModuleInfoFrame(ConstCode.MODULE_SOFTWARE_VERSION_FIELD));
		}

		private static void getModuleRegion()
		{
			Sp.GetInstance().Send(Commands.BuildFrame(ConstCode.FRAME_TYPE_CMD, "08"));

		}

		private static void adjustUIcomponents(string hardwareVersion)
		{
			if (hardwareVersion.Length >= 10 && "M100 26dBm".Equals(hardwareVersion.Substring(0, 10)))
			{


				AntennaGain = "3";
				Coupling = "-20";

			}
			else if (hardwareVersion.Length >= 10 && "M100 20dBm".Equals(hardwareVersion.Substring(0, 10)))
			{

				AntennaGain = "1";
				Coupling = "-27";

			}
			else if (hardwareVersion.Length >= 10 && "QM100 30dBm".Equals(hardwareVersion.Substring(0, 11)))
			{

				AntennaGain = "3";
				Coupling = "-10";

			}
			else if (hardwareVersion.Length >= 5 && "QM100".Equals(hardwareVersion.Substring(0, 5)))
			{

				AntennaGain = "4";
				Coupling = "-10";

			}
		}
	}
}
